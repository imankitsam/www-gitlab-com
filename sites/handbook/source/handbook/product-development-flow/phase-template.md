### Track Phase X: Phase Name

Label: `~label`

#### Key Participants
<!-- this table outlines the individuals contributing (or informed) for this phase. DRI - Directly Responsible Individual, Collaborators - Active participation in the phase, Informed - kept up to date on the outcomes -->

| Role |Function|
|---|---|
| DRI | TBD |
| Collaborators | TBD|
| Informed | TBD |

#### Description
Phase description...
<!-- The description should capture the intent of the phase. Why does it exist and what are the basic motions involved with the phase? Be clear and write with brevity. 

example from build > launch:

"After launch, the Product Manager and Product Designer should pay close attention to product usage data. This starts by ensuring your AMAU is instrumented and reporting as you expect. From there consider how the feature has impacted GMAU and SMAU. At this point you should also solicit customer feedback to guide follow-on iterative improvements, until success metrics are achieved/exceeded and a decision can be made that the product experience is sufficient. To create a combined and ongoing quantitative and qualitative feedback loop, the following activities are recommended:"
-->

#### Outcomes and Activities
<!-- The outcomes and related activities table above should capture the outcomes and the associated activities (recommended, or required) team members should consider deploying to achieve that outcome. Start by defining the outcomes, and layer in the acclivities where appropriate. If an activity is required designate it in **bold** and use the 🔎 emoji. 
example: 
outcome = improved understanding of a customer problem
activities = problem validation research issue, customer interviews, review related issues -->
| Outcomes|Activities|
|---|---|
| Outcome 1 | Activity 1 <br> **🔎 Activity 2** <br>|

#### Required Activities
<kbd class="required">Required 🔎</kbd>

**Activity 2** - Each PM must keep issue descriptions up to date. This allows for anyone to quickly review and understand the details of an issue without reading every comment. 