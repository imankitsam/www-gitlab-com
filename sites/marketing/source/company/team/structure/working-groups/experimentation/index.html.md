---
layout: markdown_page
title: "Experimentation Working Group"
description: "The GitLab Experimentation Working Group aims to define the process for Product Groups at GitLab to self-service the definition, running, and results analysis of Product experiments on GitLab.com."
canonical_path: "/company/team/structure/working-groups/experimentation/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property | Value |
|----------|-------|
| Date Created | September 17, 2020 |
| Date Ended   | October 30, 2020 |
| Slack        | [#wg_experimentation](https://gitlab.slack.com/archives/C01BJUKUEDN) (GitLab internal) |
| Google Doc   | [Experimentation Working Group Agenda](https://drive.google.com/drive/search?q=title:%22Experimentation%20Working%20Group%22) (GitLab internal) |

## Business Goal

Define the process for Product Groups to self-service the definition, running, and results analysis of Product experiments on GitLab.com.

In order to keep the working group short-lived and our discussions focused, we are explicitly & exclusively focusing on experimentation within GitLab.com. This means that we will consider the discussion of experimentation on self-managed, customers.gitlab.com, about.gitlab.com, and other such entities as being out of scope for this working group.

## Exit Criteria (100%)

* Documentation: Experiment ideation/write-up process - how should someone think about writing up an experiment proposal, what’s required, what’s optional, what does the validation process look like, what is the success metric
* Documentation: A guide to the types of experiments supported, how these can be rolled out by user/namespace/group, and the technical process to implement this rollout.
* Documentation: How to track experiments, frontend and backend events.
* Documentation: Code cleanup process once an experiment is complete, where to write up results, how to determine which experiments are active or completed.
* A guide to the types of data that can be collected, where and how, and for what use.
* Self-service completion of 10 experiments across three separate product groups

## Roles and Responsibilities

| Working Group Role    | Person                | Title                           |
|-----------------------|-----------------------|---------------------------------|
| Facilitator           | Phil Calder           | Engineering Manager, Growth     |
| Executive Sponsor     | Anoop                 | VP Product                      |
| Product Lead          | Hila Qu               | Director, Growth                |
| Engineering Lead      | Bartek Marnane        | Director of Engineering, Growth |
| Member                | Sam Awezec            | Senior PM, Growth               |
| Member                | Kenny Johnston        | Senior Director, Product        |
| Member                | Jerome Ng             | Acting PM, Telemetry            |
| Member                | Nicolas Dular         | Senior Fullstack Engineer       |
| Member                | Dallas Reedy          | Fullstack Engineer              |
| Member                | Jeremy Jackson        | Senior Fullstack Engineer       |
| Member                | Kathleen Tam          | Manager, Data                   |
